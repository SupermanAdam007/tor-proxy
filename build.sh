#!/bin/bash
# versions are from here: http://www.haproxy.org/download/2.0/src/

docker build -t tor-proxy:latest \
    --build-arg HAPROXY_MAJOR="1.8" \
    --build-arg HAPROXY_VERSION="1.8.20" \
    --build-arg HAPROXY_MD5="abf9b7b1aa84e0839501e006fc20d7fd" \
    .
