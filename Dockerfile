FROM alpine:3.10

# install HAproxy
ARG HAPROXY_MAJOR
ARG HAPROXY_VERSION
ARG HAPROXY_MD5
ARG WITH_LUA

COPY scripts/buildHAproxy.sh /
RUN chmod +x /buildHAproxy.sh
RUN ./buildHAproxy.sh

# install tor
RUN echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories && \
    apk -U upgrade && \
    apk -v add tor@edge curl bash && rm -rf /var/cache/apk/*

RUN mkdir -p /var/run/tor /var/db/tor/1 /var/db/tor/2 /var/db/tor/3
RUN chmod -R 700 /var/db/tor /var/run/tor

COPY config/haproxy.conf /etc/haproxy/haproxy.conf

EXPOSE 9100

COPY scripts/run.sh /
RUN chmod +x /run.sh
WORKDIR /

CMD ["./run.sh"]
