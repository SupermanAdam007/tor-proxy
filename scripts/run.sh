#!/usr/bin/env sh
# Reads HOW_MANY_TORS env variable and run that number of tors instances  
set -ex

#HOW_MANY_TORS=$1

echo "Running tor instances..."

/usr/bin/tor --SocksPort 0.0.0.0:41000 --HTTPTunnelPort 0.0.0.0:40000 --MaxCircuitDirtiness 60 --PidFile /var/run/tor/1.pid --RunAsDaemon 1 --DataDirectory /var/db/tor/1 --allow-missing-torrc
/usr/bin/tor --SocksPort 0.0.0.0:41001 --HTTPTunnelPort 0.0.0.0:40001 --MaxCircuitDirtiness 60 --PidFile /var/run/tor/2.pid --RunAsDaemon 1 --DataDirectory /var/db/tor/2 --allow-missing-torrc
/usr/bin/tor --SocksPort 0.0.0.0:41002 --HTTPTunnelPort 0.0.0.0:40002 --MaxCircuitDirtiness 60 --PidFile /var/run/tor/3.pid --RunAsDaemon 1 --DataDirectory /var/db/tor/3 --allow-missing-torrc

sleep 5
echo "Run HAproxy"
haproxy -f /etc/haproxy/haproxy.conf -p /run/haproxy.pid
#/usr/sbin/haproxy-systemd-wrapper -p /run/haproxy.pid -f /etc/haproxy/haproxy.conf
