# Tor proxy

Container, Tor, HAProxy

## Getting started
```
docker build -t tor-proxy:latest \
    --build-arg HAPROXY_MAJOR="1.8" \
    --build-arg HAPROXY_VERSION="1.8.20" \
    --build-arg HAPROXY_MD5="abf9b7b1aa84e0839501e006fc20d7fd" \
    .

docker run --rm -it -p 9100:9100 tor-proxy

curl --proxy "http://localhost:9100" "http://httpbin.org/ip"
```
